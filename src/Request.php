<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

class Request
{
    /** @var array<string, mixed> */
    private array $data;

    /**
     * @param  array<int, Item>  $items
     */
    public function __construct(
        Client $client,
        Address $origin,
        Address $destination,
        Address $billing,
        array $items,
        DeliveryType $deliveryType = DeliveryType::Commercial,
        bool $overlength = false,
        bool $singleShipment = false,
    ) {
        $this->data = [
            'version' => '1.0',
            'shipmentType' => 'LTL',
            'shipmentMode' => 'Dry Van',
            'paymentType' => 'Outbound Prepaid',
            'client' => [
                'id' => $client->kuebixClientId(),
            ],
            'origin' => [
                'companyName' => $origin->company,
                'streetAddress' => $origin->street,
                'city' => $origin->city,
                'stateProvince' => $origin->region,
                'postalCode' => $origin->postal,
                'country' => $origin->country,
            ],
            'destination' => [
                'companyName' => $destination->company,
                'streetAddress' => $destination->street,
                'city' => $destination->city,
                'stateProvince' => $destination->region,
                'postalCode' => $destination->postal,
                'country' => $destination->country,
            ],
            'billTo' => [
                'companyName' => $billing->company,
                'streetAddress' => $billing->street,
                'city' => $billing->city,
                'stateProvince' => $billing->region,
                'postalCode' => $billing->postal,
                'country' => $billing->country,
            ],
            'weightUnit' => 'LB',
            'lengthUnit' => 'IN',
        ];

        $lineItems = [];
        $handlingUnits = [];

        foreach ($items as $item) {
            $lineItems[] = [
                'sku' => $item->sku,
                'quantity' => $item->quantity,
                'weight' => $item->weight * $item->quantity,
                'freightClass' => $item->freightClass,
                'nmfc' => $item->nmfc,
            ];

            $handlingUnits[] = [
                'quantity' => $item->quantity,
                'huType' => 'Skid(s)',
                'length' => $item->length,
                'width' => $item->width,
                'height' => $item->height,
                'weight' => $item->weight * $item->quantity,
            ];
        }

        $this->data['lineItems'] = $lineItems;
        $this->data['handlingUnits'] = $handlingUnits;

        $accessorials = match ($deliveryType) {
            DeliveryType::Residential => [
                [
                    'code' => 'LGATE',
                    'accessorialType' => 'Delivery',
                ],
                [
                    'code' => 'RESIDNTL',
                    'accessorialType' => 'Delivery',
                ],
            ],
            DeliveryType::Special => [
                [
                    'code' => 'LTDACCESS',
                    'accessorialType' => 'Delivery',
                    'parameters' => [
                        [
                            'name' => 'accessType',
                            'value' => 'FARM',
                        ],
                    ],
                ],
            ],
            DeliveryType::Commercial => [],
        };

        if ($overlength) {
            $dimensions = array_map(
                function ($item) {
                    return max(
                        $item->length,
                        $item->height,
                        $item->width,
                    );
                },
                $items,
            );

            $maxDimension = ceil(max($dimensions) / 12);

            $accessorials[] = [
                'code' => 'OVERLENGTH',
                'accessorialType' => 'Other',
                'parameters' => [
                    [
                        'name' => 'length',
                        'value' => $maxDimension,
                    ],
                ],
            ];
        }

        if ($singleShipment) {
            $accessorials[] = [
                'code' => 'SINGLE',
                'accessorialType' => 'Other',
            ];
        }

        if (count($accessorials) > 0) {
            $this->data['accessorials'] = $accessorials;
        }
    }

    /** @return array<string, mixed> */
    public function data(): array
    {
        return $this->data;
    }

    public function json(): string
    {
        return json_encode($this->data, JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
    }
}
