<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

class Item
{
    public function __construct(
        public readonly string $sku,
        public readonly string $freightClass,
        public readonly string $nmfc,
        public readonly int $length,
        public readonly int $width,
        public readonly int $height,
        public readonly float $weight,
        public readonly int $quantity = 1,
    ) {}
}
