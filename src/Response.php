<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

use Psr\Http\Message\ResponseInterface;

use function SlyFoxCreative\Utilities\assert_array;

class Response
{
    public function __construct(private ResponseInterface $data) {}

    public function json(): string
    {
        return (string) $this->data->getBody();
    }

    /** @return array<int|string, mixed> */
    public function data(): array
    {
        $data = json_decode($this->json(), true);
        assert_array($data);
        return $data;
    }

    public function minimumCost(): float
    {
        $response = $this->data();
        assert_array($response['rates']);

        $rates = array_filter(
            $response['rates'],
            function ($rate) {
                return ! isset($rate['errorMessage']);
            },
        );

        $rates = array_map(
            function ($rate) {
                return $rate['totalPrice'];
            },
            $rates,
        );

        return min($rates);
    }
}
