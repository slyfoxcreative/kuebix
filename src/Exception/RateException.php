<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Exception;

class RateException extends RequestException {}
