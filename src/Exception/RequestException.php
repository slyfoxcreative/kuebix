<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Exception;

use GuzzleHttp\Exception\RequestException as GuzzleRequestException;

class RequestException extends \Exception
{
    public static function fromGuzzleException(GuzzleRequestException $exception): self
    {
        $data = json_decode(strval($exception->getResponse()?->getBody()), true);

        if (is_array($data) && isset($data['errors'])) {
            $code = $exception->getCode();
            $message = implode(' / ', $data['errors']);
        } elseif (is_array($data) && isset($data['errorCode'], $data['errorStatus'])) {
            $code = intval($data['errorCode']);
            $message = $data['errorStatus'];
        } else {
            $code = $exception->getCode();
            $message = $exception->getMessage();
        }

        return static::fromErrorCode($code, $message);
    }

    public static function fromErrorCode(int $code, string $message): self
    {
        switch ($code) {
            case 401:
                return new AuthorizationException($message);

            case 600:
                return new RateException($message);

            case 622:
                return new SchemaException($message);

            default:
                return new RequestException($message);
        }
    }
}
