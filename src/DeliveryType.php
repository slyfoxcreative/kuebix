<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

enum DeliveryType: string
{
    case Commercial = 'commercial';
    case Residential = 'residential';
    case Special = 'special';

    public static function fromMagentoCode(string $code): self
    {
        if ($code === 'stock' || $code === 'dropship') {
            return self::Commercial;
        }

        try {
            return self::from($code);
        } catch (\ValueError $e) {
            throw new \ValueError("Invalid Magento code '{$code}'");
        }
    }
}
