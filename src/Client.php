<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use Psr\Log\LoggerInterface;
use SlyFoxCreative\Kuebix\Exception\RequestException;

class Client
{
    public function __construct(
        private string $kuebixClientId,
        private string $username,
        private string $apiKey,
        private string $endpointUrl,
        private ?LoggerInterface $logger = null,
    ) {}

    public function request(Request $request): Response
    {
        $guzzleClient = new GuzzleClient();

        if (isset($this->logger)) {
            $this->logger->debug($request->json());
        }

        try {
            $guzzleResponse = $guzzleClient->post(
                $this->endpointUrl,
                [
                    'auth' => [
                        $this->username,
                        $this->apiKey,
                    ],
                    'json' => $request->data(),
                ],
            );
        } catch (GuzzleRequestException $e) {
            throw RequestException::fromGuzzleException($e);
        }

        $response = new Response($guzzleResponse);
        $data = $response->data();

        if (array_key_exists('error', $data) && $data['error']) {
            throw RequestException::fromErrorCode(intval($data['errorCode']), $data['errorStatus']);
        }

        if (! array_key_exists('rates', $data)) {
            throw RequestException::fromErrorCode(600, 'No rates');
        }

        if (isset($this->logger)) {
            $this->logger->debug($response->json());
        }

        return $response;
    }

    public function kuebixClientId(): string
    {
        return $this->kuebixClientId;
    }
}
