<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix;

class Address
{
    public function __construct(
        public readonly string $street,
        public readonly string $city,
        public readonly string $region,
        public readonly string $postal,
        public readonly string $country,
        public readonly string $company = '',
    ) {}
}
