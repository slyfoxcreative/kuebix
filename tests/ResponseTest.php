<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\Address;
use SlyFoxCreative\Kuebix\Client;
use SlyFoxCreative\Kuebix\Item;
use SlyFoxCreative\Kuebix\Request;
use SlyFoxCreative\Kuebix\Response;

class ResponseTest extends TestCase
{
    protected Client $client;

    protected Address $origin;

    protected Address $destination;

    /** @var array<int, Item> */
    protected array $items;

    protected Request $request;

    protected Response $response;

    protected function setUp(): void
    {
        $this->client = new Client(
            $_ENV['KUEBIX_CLIENT_ID'],
            $_ENV['KUEBIX_USERNAME'],
            $_ENV['KUEBIX_API_KEY'],
            $_ENV['KUEBIX_ENDPOINT_URL'],
        );

        $this->origin = new Address(
            '601 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'SlyFox Creative',
        );

        $this->destination = new Address(
            '585 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'Excel Equipment',
        );

        $this->items[] = new Item(
            'TTN*HD2P-9KCL',
            '77.5',
            '111220-03',
            154,
            20,
            39,
            1466,
        );

        $this->request = new Request(
            $this->client,
            $this->origin,
            $this->destination,
            $this->destination,
            $this->items,
        );

        $this->response = $this->client->request($this->request);
    }

    public function testData(): void
    {
        $data = $this->response->data();

        self::assertArrayHasKey('rateMap', $data);
        self::assertArrayHasKey('rates', $data);
    }

    // This test is just making sure the function call doesn't throw an
    // exception, so it has no useful assertions to perform.
    #[DoesNotPerformAssertions]
    public function testMinimumCost(): void
    {
        $this->response->minimumCost();
    }
}
