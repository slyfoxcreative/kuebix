<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\Address;
use SlyFoxCreative\Kuebix\Client;
use SlyFoxCreative\Kuebix\DeliveryType;
use SlyFoxCreative\Kuebix\Item;
use SlyFoxCreative\Kuebix\Request;

class RequestTest extends TestCase
{
    protected Client $client;

    protected Address $address;

    protected Item $item;

    protected function setUp(): void
    {
        $this->client = new Client(
            $_ENV['KUEBIX_CLIENT_ID'],
            $_ENV['KUEBIX_USERNAME'],
            $_ENV['KUEBIX_API_KEY'],
            $_ENV['KUEBIX_ENDPOINT_URL'],
        );

        $this->address = new Address(
            '601 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'SlyFox Creative',
        );

        $this->item = new Item(
            'TTN*HD2P-9KCL',
            '77.5',
            '111220-03',
            154,
            20,
            39,
            1466,
        );
    }

    public function testCommercial(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_commercial.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testCommercialExtremeLength(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            overlength: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_commercial_extreme_length.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testCommercialSingle(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            singleShipment: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_commercial_single.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testResidential(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Residential,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_residential.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testResidentialExtremeLength(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Residential,
            overlength: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_residential_extreme_length.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testResidentialSingle(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Residential,
            singleShipment: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_residential_single.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testSpecialCommercial(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Special,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_special_commercial.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testSpecialCommercialExtremeLength(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Special,
            overlength: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_special_commercial_extreme_length.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testSpecialCommercialSingle(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
            deliveryType: DeliveryType::Special,
            singleShipment: true,
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_special_commercial_single.json',
            json_encode($request->data(), JSON_THROW_ON_ERROR),
        );
    }

    public function testJson(): void
    {
        $request = new Request(
            $this->client,
            $this->address,
            $this->address,
            $this->address,
            [$this->item],
        );

        self::assertJsonStringEqualsJsonFile(
            'tests/fixtures/request_commercial.json',
            $request->json(),
        );
    }
}
