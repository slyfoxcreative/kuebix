<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\Item;

class ItemTest extends TestCase
{
    protected Item $item;

    protected function setUp(): void
    {
        $this->item = new Item(
            'TTN*HD2P-9KCL',
            '77.5',
            '111220-03',
            154,
            20,
            39,
            1466,
            2,
        );
    }

    public function testFreightClass(): void
    {
        self::assertSame('77.5', $this->item->freightClass);
    }

    public function testHeight(): void
    {
        self::assertSame(39, $this->item->height);
    }

    public function testLength(): void
    {
        self::assertSame(154, $this->item->length);
    }

    public function testNMFC(): void
    {
        self::assertSame('111220-03', $this->item->nmfc);
    }

    public function testQuantity(): void
    {
        self::assertSame(2, $this->item->quantity);
    }

    public function testSku(): void
    {
        self::assertSame('TTN*HD2P-9KCL', $this->item->sku);
    }

    public function testWeight(): void
    {
        self::assertSame(1466.0, $this->item->weight);
    }

    public function testWidth(): void
    {
        self::assertSame(20, $this->item->width);
    }

    public function testNoQuantity(): void
    {
        $item = new Item(
            'TTN*HD2P-9KCL',
            '77.5',
            '111220-03',
            154,
            20,
            39,
            1466,
        );

        self::assertSame(1, $item->quantity);
    }
}
