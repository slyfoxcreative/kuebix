<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\Attributes\DoesNotPerformAssertions;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\Address;
use SlyFoxCreative\Kuebix\Client;
use SlyFoxCreative\Kuebix\Exception\AuthorizationException;
use SlyFoxCreative\Kuebix\Exception\RateException;
use SlyFoxCreative\Kuebix\Exception\RequestException;
use SlyFoxCreative\Kuebix\Item;
use SlyFoxCreative\Kuebix\Request;

class ClientTest extends TestCase
{
    protected Client $client;

    protected Address $origin;

    protected Address $destination;

    /** @var array<int, Item> */
    protected array $items;

    protected Request $request;

    protected function setUp(): void
    {
        $this->client = new Client(
            $_ENV['KUEBIX_CLIENT_ID'],
            $_ENV['KUEBIX_USERNAME'],
            $_ENV['KUEBIX_API_KEY'],
            $_ENV['KUEBIX_ENDPOINT_URL'],
        );

        $this->origin = new Address(
            '601 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'SlyFox Creative',
        );

        $this->destination = new Address(
            '585 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'Excel Equipment',
        );

        $this->items[] = new Item(
            'TTN*HD2P-9KCL',
            '77.5',
            '111220-03',
            154,
            20,
            39,
            1466,
        );

        $this->request = new Request(
            $this->client,
            $this->origin,
            $this->destination,
            $this->destination,
            $this->items,
        );
    }

    public function testKuebixClientId(): void
    {
        self::assertSame($_ENV['KUEBIX_CLIENT_ID'], $this->client->kuebixClientId());
    }

    // This test is just making sure the function call doesn't throw an
    // exception, so it has no useful assertions to perform.
    #[DoesNotPerformAssertions]
    public function testValidReponse(): void
    {
        $this->client->request($this->request);
    }

    public function testRequestException(): void
    {
        $this->expectException(RequestException::class);
        $this->expectExceptionMessage('Could not read document: Can not construct instance of com.kuebix.beans.salesforce.Id, problem: Salesforce IDs must be of length 15 or 18.');

        $client = new Client(
            'foo',
            $_ENV['KUEBIX_USERNAME'],
            $_ENV['KUEBIX_API_KEY'],
            $_ENV['KUEBIX_ENDPOINT_URL'],
        );

        $request = new Request(
            $client,
            $this->origin,
            $this->destination,
            $this->destination,
            $this->items,
        );

        $response = $client->request($request);
    }

    public function testAuthorizationException(): void
    {
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('Client error: `POST https://shipment.kuebix.com/api/action/quickRate` resulted in a `401 Unauthorized` response:');

        $client = new Client(
            $_ENV['KUEBIX_CLIENT_ID'],
            'foo@bar.com',
            $_ENV['KUEBIX_API_KEY'],
            $_ENV['KUEBIX_ENDPOINT_URL'],
        );

        $request = new Request(
            $client,
            $this->origin,
            $this->destination,
            $this->destination,
            $this->items,
        );

        $response = $client->request($request);
    }

    public function testRateException(): void
    {
        $this->expectException(RateException::class);
        $this->expectExceptionMessage('No agreements found for the account and hence there are no rates.');

        $items = [
            new Item(
                'TTN*HD2P-9KCL',
                '77.5',
                '111220-03',
                154,
                20,
                39,
                999999,
            ),
        ];

        $request = new Request(
            $this->client,
            $this->origin,
            $this->destination,
            $this->destination,
            $items,
        );

        $response = $this->client->request($request);
    }
}
