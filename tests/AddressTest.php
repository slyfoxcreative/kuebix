<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\Address;

class AddressTest extends TestCase
{
    protected Address $address;

    protected function setUp(): void
    {
        $this->address = new Address(
            '601 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
            'SlyFox Creative',
        );
    }

    public function testCity(): void
    {
        self::assertSame('Greenwood', $this->address->city);
    }

    public function testCompany(): void
    {
        self::assertSame('SlyFox Creative', $this->address->company);
    }

    public function testCountry(): void
    {
        self::assertSame('US', $this->address->country);
    }

    public function testPostal(): void
    {
        self::assertSame('46143', $this->address->postal);
    }

    public function testRegion(): void
    {
        self::assertSame('IN', $this->address->region);
    }

    public function testStreet(): void
    {
        self::assertSame('601 Sayre Ct', $this->address->street);
    }

    public function testNoCompany(): void
    {
        $address = new Address(
            '601 Sayre Ct',
            'Greenwood',
            'IN',
            '46143',
            'US',
        );

        self::assertSame('', $address->company);
    }
}
