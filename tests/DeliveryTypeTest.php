<?php

declare(strict_types=1);

namespace SlyFoxCreative\Kuebix\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Kuebix\DeliveryType;

class DeliveryTypeTest extends TestCase
{
    public function testFromMagentoCode(): void
    {
        $expectedValues = [
            'stock' => DeliveryType::Commercial,
            'dropship' => DeliveryType::Commercial,
            'commercial' => DeliveryType::Commercial,
            'residential' => DeliveryType::Residential,
            'special' => DeliveryType::Special,
        ];

        foreach ($expectedValues as $code => $expected) {
            self::assertSame($expected, DeliveryType::fromMagentoCode($code));
        }
    }

    public function testFromMagentoCodeWithInvalidCode(): void
    {
        self::expectException(\ValueError::class);
        self::expectExceptionMessage("Invalid Magento code 'invalid'");

        DeliveryType::fromMagentoCode('invalid');
    }
}
